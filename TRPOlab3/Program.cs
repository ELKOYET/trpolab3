﻿using System;
using TRPO3labBib;
using TRPOlab3;

namespace TRPOlab3
{
    class Program
    {

        static void Main(string[] args)
        {
            SquareClass sq = new SquareClass();
            try
            {
                Console.WriteLine("Сейчас ты будешь считать площадь кольца,сынок ");
                Console.WriteLine("Введи внешний радиус(он должен быть больше,чем внутренний ");
                double OutR = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Введи внутренний радиус(он должен быть меньше,чем внешний ");
                double InR = Convert.ToDouble(Console.ReadLine());
                if (OutR < InR)
                {
                    Console.WriteLine("  па руски написано меньше");
                    throw new Exception();
                }
                else
                {
                    double s = sq.GetSq(OutR, InR);
                    Console.WriteLine($"Площадь кольца = {s} ед. изм.");
                }
            }
            catch
            {
                Main(args);
            }
            Console.ReadKey();

        }
    }

}
