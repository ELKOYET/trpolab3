﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TRPOASP.Models;
using TRPO3labBib;
using System.ComponentModel.DataAnnotations;

namespace TRPOASP.Controllers
{
    public class HomeController : Controller
    {
       

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
     
        public ActionResult Index( string OutR,string InR)
        {

            double Square =  new SquareClass().GetSq(Convert.ToDouble(OutR), Convert.ToDouble(InR));
            if (Square >= 0)
            {
                ViewBag.result = Square;
            }
            else 
            {
                ViewBag.result = "Внешний радиус не может быть меньше внутреннего";
            }
           
            return View();
        }

       
        
    }
}
