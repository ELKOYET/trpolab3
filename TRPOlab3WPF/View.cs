﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using TRPO3labBib;

namespace TRPOlab3WPF
{
    public class View : INotifyPropertyChanged
    {
        SquareClass square = new SquareClass();


        public static double GetSq(double OutR, double InR)
        {
            if (OutR >= InR)
            {
                return new SquareClass().GetSq(OutR, InR);
            }
            else
            {
                throw new ArgumentException("Внешний радиус не может быть меньше внутреннего");
            }

        }

        private double _OutR;
        public double OutR
        {
            get { return _OutR; }
            set
            {
                if (double.IsNegative(value))

                    throw new ArgumentException("да да ");

                _OutR = value;
                OnPropertyChanged("S");
            }
        }
        private double _InR;
        public double InR
        {
            get { return _InR; }
            set
            {
                _InR = value;
                OnPropertyChanged("S");
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public string S { get =>  View.GetSq(OutR, InR).ToString();  }

    }
}

