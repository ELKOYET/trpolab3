﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;


namespace TRPO3labBib
{
    public class SquareClass: INotifyPropertyChanged
    {
        public SquareClass() { }
       
        public  double GetSq(double OutR, double InR) => Math.PI * (Math.Pow(OutR, 2) - Math.Pow(InR, 2));
        
        private double _OutR;
        public double OutR { get { return _OutR; } 
            set
            {
                _OutR = value;
                OnPropertyChanged("S");
            }
        }
        private double _InR;
        public double InR { get { return _InR; }
            set 
            {
                _InR = value;
                OnPropertyChanged("S");
            }        
        }
      

       
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public double S { get {return this.GetSq(OutR, InR); } }

    }
        
    
}
